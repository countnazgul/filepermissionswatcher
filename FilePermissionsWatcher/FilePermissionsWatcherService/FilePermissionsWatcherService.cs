﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
//using System.Linq;
using System.ServiceProcess;
//using System.Text;
using System.Configuration;
using System.Xml;
using System.Xml.XPath;
using System.Security.AccessControl;
using System.IO;
using System.Threading;

namespace FilePermissionsWatcherService
{
    public partial class FilePermissionsWatcher : ServiceBase
    {
        public FilePermissionsWatcher()
        {
            InitializeComponent();
        }

        string folder, extension, userOrGroup, skip;

        protected override void OnStart(string[] args)
        {
            GetSettings();
            fileSystemWatcher1.Path = folder;
            fileSystemWatcher1.Filter = extension;
            fileSystemWatcher1.EnableRaisingEvents = true;
        }

        protected override void OnStop()
        {
        }

        private void fileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {
            AccessControlType access = new AccessControlType();
            ChangePermissions(e.FullPath, 0, access, access);
        }

        private void fileSystemWatcher1_Created(object sender, FileSystemEventArgs e)
        {
            AccessControlType access = new AccessControlType();
            ChangePermissions(e.FullPath, 0, access, access);
        }

        private void fileSystemWatcher1_Renamed(object sender, RenamedEventArgs e)
        {
            AccessControlType access = new AccessControlType();
            ChangePermissions(e.FullPath, 0, access, access);
        }

        public void GetSettings()
        {    
            try {
                XmlDocument document = new XmlDocument();
                document.Load(@"c:\Program Files (x86)\FilePermissionsWatcher\config.xml");

                XmlNode folderNode = document.SelectSingleNode("/settings/folder");
                folder = folderNode.InnerText;

                XmlNode usergroupNode = document.SelectSingleNode("/settings/usergroup");
                userOrGroup = usergroupNode.InnerText;

                XmlNode extensionNode = document.SelectSingleNode("/settings/extension");
                extension = extensionNode.InnerText;

                XmlNode skipNode = document.SelectSingleNode("/settings/skip");
                skip = skipNode.InnerText;
            }
            catch(Exception ex)
            {
                EventLog log = new System.Diagnostics.EventLog();
                log.Source = this.ServiceName;
                log.Log = "Application";
                log.WriteEntry(ex.Message, EventLogEntryType.Error);                 
            }         
        }

        private void ChangePermissions(string filePath, int manual, AccessControlType removeAccess, AccessControlType addAccess)
        {
            try
            {
                if (filePath.ToLower() != skip.ToLower())
                {
                    FileSecurity security = File.GetAccessControl(filePath);
                    FileSystemAccessRule rule = new FileSystemAccessRule(userOrGroup, FileSystemRights.FullControl, AccessControlType.Allow);
                    security.RemoveAccessRule(rule);
                    File.SetAccessControl(filePath, security);
                    rule = new FileSystemAccessRule(userOrGroup, FileSystemRights.FullControl, AccessControlType.Deny);
                    security.AddAccessRule(rule);
                    File.SetAccessControl(filePath, security);
                }
            }
            catch (Exception ex)
            {                
                EventLog log = new System.Diagnostics.EventLog();
                log.Source = this.ServiceName;
                log.Log = "Application";
                log.WriteEntry(ex.Message, EventLogEntryType.Error);
            }

        }
    }
}
