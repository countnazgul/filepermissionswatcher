﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Configuration.Install;
using System.Reflection;

namespace FilePermissionsWatcherService
{
 
  static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            
            if (Environment.UserInteractive)
            {
                string parameter = string.Concat(args);
                switch (parameter)
                {
                    case "--install":
                        ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                        break;
                    case "--uninstall":
                        ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                        break;
                }
            }
            else
            {
                ServiceBase[] servicesToRun = new ServiceBase[] 
                          { 
                              new FilePermissionsWatcher() 
                          };
                ServiceBase.Run(servicesToRun);
            }
            

            
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new FilePermissionsWatcher() 
			};
            ServiceBase.Run(ServicesToRun);
             
        }
    }


/*
    static class Program
    {
        static void Main()
        {
#if(!DEBUG)
           ServiceBase[] ServicesToRun;
           ServicesToRun = new ServiceBase[] 
	   { 
	        new MyService() 
	   };
           ServiceBase.Run(ServicesToRun);
#else
            FilePermissionsWatcher myServ = new FilePermissionsWatcher();
            myServ.GetSettings();
            // here Process is my Service function
            // that will run when my service onstart is call
            // you need to call your own method or function name here instead of Process();
#endif
        }
    }
*/

}
