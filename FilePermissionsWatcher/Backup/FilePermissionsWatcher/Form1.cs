﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Security.AccessControl;
using System.DirectoryServices;
using System.Drawing;

namespace FilePermissionsWatcher
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnStart.Text = "Start";
            fileSystemWatcher1.EnableRaisingEvents = false;
            lblError.Text = "";
            lblErrorManual.Text = "";
            cbPermissions.SelectedIndex = 1;

            this.mynotifyicon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info; //Shows the info icon so the user doesn't thing there is an error.
            this.mynotifyicon.BalloonTipText = "Minimimzed to tray";
            this.mynotifyicon.BalloonTipTitle = "Permissions";
            GetUsers();
            cbUsrOrGroup.SelectedIndex = 0;
            cbUsrOrGroupFile.SelectedIndex = 0;
        }

        private void fileSystemWatcher1_Changed(object sender, FileSystemEventArgs e)
        {
            AccessControlType access = new AccessControlType();
            ChangePermissions(e.FullPath, 0, access, access);
        }

        private void fileSystemWatcher1_Created(object sender, FileSystemEventArgs e)
        {
            AccessControlType access = new AccessControlType();
            ChangePermissions(e.FullPath, 0, access, access);
        }

        private void fileSystemWatcher1_Renamed(object sender, RenamedEventArgs e)
        {
            AccessControlType access = new AccessControlType();
            ChangePermissions(e.FullPath, 0, access, access);
        }

        private void ChangePermissions(string filePath, int manual, AccessControlType removeAccess, AccessControlType addAccess)
        {
            if (manual != 1)
            {
                try
                {
                    if (filePath.ToLower() != txtSkipFile.Text.ToLower())
                    {
                        string userOrGroup = cbUsrOrGroup.SelectedItem.ToString();
                        FileSecurity security = File.GetAccessControl(filePath);
                        FileSystemAccessRule rule = new FileSystemAccessRule(userOrGroup, FileSystemRights.FullControl, AccessControlType.Allow);
                        security.RemoveAccessRule(rule);
                        File.SetAccessControl(filePath, security);
                        rule = new FileSystemAccessRule(userOrGroup, FileSystemRights.FullControl, AccessControlType.Deny);
                        security.AddAccessRule(rule);
                        File.SetAccessControl(filePath, security);
                    }
                }
                catch (Exception ex)
                {
                    using (StreamWriter writer = new StreamWriter("filewatcher.log", true))
                    {
                        writer.WriteLine(ex.Message);
                    }
                }
            }
            else
            {
                try
                {
                    string userOrGroup = cbUsrOrGroupFile.SelectedItem.ToString();
                    FileSecurity security = File.GetAccessControl(filePath);
                    FileSystemAccessRule rule = new FileSystemAccessRule(userOrGroup, FileSystemRights.FullControl, removeAccess);
                    security.RemoveAccessRule(rule);
                    File.SetAccessControl(filePath, security);
                    rule = new FileSystemAccessRule(userOrGroup, FileSystemRights.FullControl, addAccess);
                    security.AddAccessRule(rule);
                    File.SetAccessControl(filePath, security);
                }
                catch (Exception ex)
                {
                    lblErrorManual.Text = "File don't exists!";
                }
            }
        }

        private void btnFileBrowser_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            txtFolderToWatch.Text = folderBrowserDialog1.SelectedPath;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            if (btnStart.Text == "Start")
            {
                if (txtFileExtension.Text.Length > 0 && txtFolderToWatch.Text.Length > 0 && txtSkipFile.Text.Length > 0)
                {
                    try
                    {
                        btnStart.Text = "Stop";
                        fileSystemWatcher1.Path = txtFolderToWatch.Text;
                        fileSystemWatcher1.Filter = txtFileExtension.Text;
                        fileSystemWatcher1.EnableRaisingEvents = true;
                        lblError.Text = "";
                        this.mynotifyicon.Text = "Running!";
                        txtFolderToWatch.Enabled = false;
                        txtFileExtension.Enabled = false;
                        cbUsrOrGroup.Enabled = false;
                        txtSkipFile.Enabled = false;
                    }
                    catch (Exception ex) {
                        lblError.Text = ex.Message;
                    }
                    
                }
                else
                {
                    lblError.Text = "Not all values are present!";
                }
            }
            else
            {
                btnStart.Text = "Start";
                fileSystemWatcher1.EnableRaisingEvents = false;
                this.mynotifyicon.Text = "Stoped!";
                txtFolderToWatch.Enabled = true;
                txtFileExtension.Enabled = true;
                cbUsrOrGroup.Enabled = true;
                txtSkipFile.Enabled = true;
            }
        }

        private void btnBrowseFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFileName.Text = openFileDialog1.FileName;
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            if (txtFileName.Text.Length > 0)
            {
                AccessControlType addAccess;
                AccessControlType removeAccess;

                if(cbPermissions.SelectedItem.ToString() == "Allow") 
                {
                    addAccess = AccessControlType.Allow;
                    removeAccess = AccessControlType.Deny;
                }
                else
                {
                    addAccess = AccessControlType.Deny;
                    removeAccess = AccessControlType.Allow;
                }

                lblErrorManual.Text = "";
                ChangePermissions(txtFileName.Text, 1, removeAccess, addAccess);
            }
            else
            {
                lblErrorManual.Text = "Not all values are present!";
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                mynotifyicon.Visible = true;
                mynotifyicon.ShowBalloonTip(3000);
                this.ShowInTaskbar = false;
            }
        }

        private void mynotifyicon_DoubleClick(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            mynotifyicon.Visible = false;
        }

        private void btnSkipFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtSkipFile.Text = openFileDialog1.FileName;
        }

        private void GetUsers()
        {
            var path = string.Format("WinNT://{0},computer", Environment.MachineName);

            using (var computerEntry = new DirectoryEntry(path))
            {
                var userNames = from DirectoryEntry childEntry in computerEntry.Children
                                where (childEntry.SchemaClassName == "User")
                                select childEntry.Name;

                foreach (var name in userNames)
                {
                    cbUsrOrGroup.Items.Add(Environment.MachineName + "\\" + name);
                    cbUsrOrGroupFile.Items.Add(Environment.MachineName + "\\" + name);
                }
            }

            using (var computerEntry = new DirectoryEntry(path))
            {
                var userNames = from DirectoryEntry childEntry in computerEntry.Children
                                where (childEntry.SchemaClassName == "Group")
                                select childEntry.Name;

                foreach (var name in userNames)
                {
                   
                   cbUsrOrGroup.Items.Add(Environment.MachineName + "\\" + name);
                    cbUsrOrGroupFile.Items.Add(Environment.MachineName + "\\" + name);
                }
            }
        }

    }
}
